# README #

This README describes steps that are necessary to get your application up and running.

### What is this repository for? ###

This is an art blog that allows users to share their thoughts about different art movements, such as painting, writing, music. It is possible both to add your own piece of art or to write a review on the existing one.

A user is able to add comments and share his/her opinion by clicking on like/dislike buttons

### How do I get set up? ###

first you need to clone this repository

```
git clone https://yulia_danilova@bitbucket.org/yulia_danilova/blog.git
```

check that it's installed to your directory

```
ls
```

go to the directory 'blog'

```
cd blog
```

this blog requires python>=3.4, django>=1.9 and some django packages installed
you can check the full list of dependencies in 'requirements.txt'
to install those you can use pip

```
pip install -r requirements.txt
```

then you can run the server locally

```
python manage.py runserver
```

you will find the link for accessing the blog in the terminal

### reCaptcha

This blog lets verify if a user is a human using reCaptcha

To do that, you have to get a pair of private and public keys on the website
https://www.google.com/recaptcha/intro/index.html

then open settings.py in blog directory

find these lines

```
RECAPTCHA_PUBLIC_KEY = '6Lc_6AcUAAAAAO0gyB48j3feyus6PDIXXXXX'

RECAPTCHA_PRIVATE_KEY = '6Lc_6AcUAAAAACh1fP3mn5dGXQ16F7yiLXXXXX'
```

and put your data

change USES_CAPTCHA variable to True