from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from posts.models import Category, Post, Comment
from posts.forms import PostForm, CommentForm

def index(request):
    posts = Post.objects.all().order_by('-created_date')
    context_dict = {
        'posts': posts
    }
    return render(request, 'posts/index.html', context=context_dict)


def category(request, category):
    try:
        requested_category = Category.objects.get(name=category)
        posts = Post.objects.filter(category__name=category).order_by('-created_date')
        context_dict = {
            'posts': posts
        }
        return render(request, 'posts/index.html', context=context_dict)
    except:
        return render(request, 'registration/error.html', context={
            'message': 'The requested category does not exist'
        })


def post(request, post_id):
    try:
        post = Post.objects.get(id=post_id)
        voted = False
        post_votes = post.get_users_voted()
        user_id = request.user.id
        if user_id is not None:
            if user_id in post_votes['up'] or user_id in post_votes['down']:
                voted = True
        comment_form = CommentForm()
        comments = Comment.objects.filter(post__id=post_id).order_by('-created_date')
        context_dict = {
            'post': post,
            'voted': voted,
            'comments': comments,
            'comment_form':comment_form
        }
        return render(request, 'posts/post.html', context=context_dict)
    except:
        return render(request, 'registration/error.html', context={
            'message': 'Oooops! Probably there is no such post! Would like to make one?'
        })

def userPosts(request, username):
    posts = Post.objects.filter(author__username=username).order_by('-created_date')
    context_dict = {
        'posts': posts
    }
    return render(request, 'posts/index.html', context=context_dict)

@login_required(login_url='/registration/login/')
def add_post(request):
    form = PostForm()
    if request.method == 'POST':
        form = PostForm(data=request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return index(request)
        else:
            print(form.errors)
    return render(request, 'posts/add_post.html', dict(form=form))

def add_comment(request, post_id):
    post = get_object_or_404(Post, id=post_id)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            comment.author = request.user
            comment.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        form = CommentForm()
    post(request, post_id)

def increase_likes(request, post_id):
    post = Post.objects.get(id=post_id)
    voting = post.get_users_voted()
    user_id = request.user.id
    if user_id is not None:
        voting['up'].append(user_id)
        post.set_users_voted(voting)
    post.likes += 1
    post.save()
    return HttpResponse(post.likes)

def increase_dislikes(request, post_id):
    post = Post.objects.get(id=post_id)
    voting = post.get_users_voted()
    user_id = request.user.id
    if user_id is not None:
        voting['down'].append(user_id)
        post.set_users_voted(voting)
    post.dislikes += 1
    post.save()
    return HttpResponse(post.dislikes)