from django.conf.urls import url
from posts import views

urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'^categories/(?P<category>[a-z A-Z 0-9]+)/$', views.category, name='category'),
	url(r'^users/(?P<username>[a-z A-Z 0-9]+)/$', views.userPosts, name='user_posts'),
	url(r'^add_post/$', views.add_post, name='add_post'),
	url(r'^post/(?P<post_id>[a-z A-Z 0-9]+)', views.post, name='post'),
	url(r'^add_comment/(?P<post_id>\d+)/$', views.add_comment, name='add_comment'),
	url(r'^increase_likes/(?P<post_id>\d+)/$', views.increase_likes, name='increase_likes'),
	url(r'^increase_dislikes/(?P<post_id>\d+)/$', views.increase_dislikes, name='increase_dislikes'),
]