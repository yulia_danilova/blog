from __future__ import unicode_literals

from django.db import models
import json
from django.utils import timezone
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField
from sanitizer.models import SanitizedCharField, SanitizedTextField


class Category(models.Model):
    name = models.CharField(max_length=128, unique=True)

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

class Post(models.Model):
    author = models.ForeignKey(User)
    category = models.ForeignKey(Category)
    title = SanitizedCharField(max_length=128)
    text = RichTextField()
    views = models.IntegerField(default=0)
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    users_voted = models.TextField(default='{"up":[], "down":[]}')
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def set_users_voted(self, x):
        self.users_voted = json.dumps(x)
        self.save()

    def get_users_voted(self):
        return json.loads(self.users_voted)

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.name

class Comment(models.Model):
    post = models.ForeignKey('Post')
    author = models.ForeignKey(User)
    text = SanitizedTextField()
    created_date = models.DateTimeField(default=timezone.now)
    approved_comment = models.BooleanField(default=False)

    def approve(self):
        self.approved_comment = True
        self.save()

    def __str__(self):
        return self.text
