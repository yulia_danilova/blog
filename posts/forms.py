from django import forms
from posts.models import Category, Post, Comment
from ckeditor.widgets import CKEditorWidget

class PostForm(forms.ModelForm):
    category = forms.ModelChoiceField(queryset=Category.objects.all())
    title = forms.CharField(max_length=128)
    views = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    likes = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    text = forms.CharField(widget=CKEditorWidget())
    # An inline class to provide additional information on the form.
    class Meta:
        model = Post
        fields = ('category', 'title', 'text', 'views', 'likes')


class CommentForm(forms.ModelForm):
    text = forms.CharField(widget=CKEditorWidget()) 
    class Meta:
        model = Comment
        fields = ('text',)