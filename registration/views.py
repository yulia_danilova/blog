from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse 
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect

from registration.forms import UserForm, UserProfileForm
from django.contrib.auth.models import User

def index(request):
    context_dict = {'message': 'type in your details'}
    return render(request, 'registration/index.html', context=context_dict)

def register(request):
    registered = False
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        profile_form = UserProfileForm(data=request.POST)
        if user_form.is_valid() and profile_form.is_valid(): 
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            profile = profile_form.save(commit=False) 
            profile.user = user
            if 'picture' in request.FILES:
                profile.picture = request.FILES['picture'] 
            profile.save()
            registered = True
        else:
            print(user_form.errors, profile_form.errors)
    else:
        user_form = UserForm()
        profile_form = UserProfileForm()
    return render(request, 'registration/register.html',
                      {'user_form': user_form,
                       'profile_form': profile_form,
                       'registered': registered})

def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return redirect('/posts/')
            else:
                return HttpResponse("Your account is disabled.")
        else:
            return render(request, 'registration/login.html', {'message': 'Invalid pair of login/password'})
    else:
        return render(request, 'registration/login.html', {'message': ''})

@login_required
def user_logout(request):
    logout(request)
    return redirect('/registration/login/')

def user_profile(request, user_id):
    user = User.objects.get(id=user_id)
    return render(request, 'registration/profile.html', context={'user_profile': user})

def my_profile(request):
    user = request.user
    return render(request, 'registration/profile.html', context={'user_profile': user})
