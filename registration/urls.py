from django.conf.urls import url
from registration import views

urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'^register/$', views.register, name='register'),
	url(r'^login/$', views.user_login, name='login'),
	url(r'^logout/$', views.user_logout, name='logout'),
	url(r'^profile/(?P<user_id>\d+)/$', views.user_profile, name='user_profile'),
	url(r'^profile/$', views.my_profile, name='my_profile')
]