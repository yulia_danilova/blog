from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User

class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name="profile")
    picture = models.FileField(upload_to='profile_images', blank=True)
    def __str__(self):
        return self.user.username
