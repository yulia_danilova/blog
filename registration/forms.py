from django import forms
from django.contrib.auth.models import User
from registration.models import UserProfile
from captcha.fields import ReCaptchaField
from blog.settings import USES_CAPTCHA

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    if USES_CAPTCHA:
    	captcha = ReCaptchaField(attrs={'theme' : 'clean'})
    class Meta: 
        model = User
        fields = ('username', 'email', 'password')

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('picture',)
