// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

function increase_likes(post_id) {
    jQuery.ajax({
        type: "POST",
        url: '/posts/increase_likes/' + post_id + '/',
        success: function(likes) {
            $("#" + post_id + "_likes").html(likes);
            disable_buttons(post_id);
        }
    })
}

function increase_dislikes(post_id) {
    jQuery.ajax({
        type: "POST",
        url: '/posts/increase_dislikes/' + post_id + '/',
        success: function(dislikes) {
            $("#" + post_id + "_dislikes").html(dislikes);
            disable_buttons(post_id);
        }
    })
}

function disable_buttons(post_id) {
    document.getElementById(post_id + "_likes_button").disabled = true;
    document.getElementById(post_id + "_dislikes_button").disabled = true;
}
